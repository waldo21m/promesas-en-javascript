Promesas que dependen de otras

Mejoremos el código de la lección anterior para no tener que estar anidando
funciones dentro de otras funciones. Para ello haremos lo siguiente:

function GET(url) {
    return new Promise(function(resolve, reject) {
        let ajaxCall = new XMLHttpRequest();

        ajaxCall.open('GET', url);

        ajaxCall.onload = function() {
            if (ajaxCall.status == 200) return resolve(ajaxCall.response);

            reject(Error(ajaxCall.status));
        };

        ajaxCall.onerror = function(err) {
            reject(err);
        }

        ajaxCall.send();
    });
}

function getUserInfo(username) {
    return GET("https://api.github.com/users/" + username);
}

function getRepos(repos_url) {
    return GET(repos_url);
}

let getUserPromise = getUserInfo("waldo21m");

let getReposPromise = getUserInfo("waldo21m").then(response => {
    return getRepos(JSON.parse(response).repos_url);
}).catch(console.log);

getReposPromise.then(console.log).catch(console.log);

Debemos notar que ahora las promesas las estamos guardando en variables, por
lo cual, su ejecución se ve más limpia y lineal. De manera que si tuviera que
hacer una siguiente llamada a otra petición asíncrona que dependa del resultado
de getRepos, podríamos crear otra variable... Ejemplo:

//Ejemplo #1
let getIndividualRepo = getReposPromise.then(response => {
    console.log(JSON.parse(response)[0].owner.url);
}).catch(console.log);

//Ejemplo #2
let getIndividualRepo = getReposPromise.then(response => {
    return getRepos(JSON.parse(response)[0].owner.url);
}).catch(console.log);

getIndividualRepo.then(console.log).catch(console.log);

En el ejemplo #1 podemos ver en consola la url del dueño del repositorio y en
ejemplo #2 visualizamos nuevamente getUserInfo pero es un buen ejemplo para
ver como una promesa depende de otra.

Aquí una de las cosas importantes son las siguientes:
Dentro del then de getUserInfo estamos retornando otra promesa. Y este valor de
retorno se asigna al valor que esté a la izquierda de toda la promesa por 
completo. Eso es algo muy interesante y peculiar de las promises. De esta forma
podemos seguir encadenando promesas una tras otra. Esta versión está menos
propensa a que vayamos anidando nuestro código de manera que se vuelva menos
legible.

Para recapitular, aprendimos que podemos en una variable normal almacenar las
promesas y aprendimos que lo que sea que retorne de then se puede almacenar
en una variable aunque este se encuentre fuera de la función que estamos 
asignando al manejador then. En este ejemplo, dentro de la promesa estamos
retornando otra promesa que luego más abajo podemos mostrar la respuesta
o los manejadores para la segunda promesa en lugar de ponerlos uno dentro de
otro.