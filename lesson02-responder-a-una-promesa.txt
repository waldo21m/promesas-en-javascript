Responder a una promesa

 Cuando creamos una promesa, el objeto resultante o el objeto promesa puede estar
 en uno de tres distintos estados:
    - Pendiente (pending): estado inicial, no cumplida o rechazada.
    - Cumplida (fulfilled): significa que la operación se completó satisfactoriamente.
    - Rechazada (rejected): significa que la operación falló.

Cuando nosotros creamos la promesa, nosotros definimos el estado dependiendo de
si ejecutamos el método resolve o rejected. Cuando ejecutamos el método resolve
indicamos que la promesa debe de pasar al estado cumplido y cuando ejecutamos el
método rejected significa que estamos pasando la promesa a fallido o rechazado.

Una vez que el estado de la promesa cambia de pendiente a cumplido o rechazado,
se puede recibir la información resultante utilizando el método then y el método
catch. En realidad podemos utilizar el método then pasandole dos funciones,
la primera una función que se va a ejecutar si la promesa cambió el estado a
cumplido y la segunda, una función que se va a ejecutar en caso de que la 
promesa haya pasado a un estado de rechazo.

Cuando la promesa pasa a un estado de cumplido, obtenemos el valor resultante
de la operación como argumento de la primera función, Ejemplo:

let promise = new Promise(function(resolve, reject) {
    setTimeout(function() {
        if (confirm("¿Esta promesa se cumplió?"))
            return resolve("Hola mundo");
        return reject(new Error("Hubo un error"));
    }, 2000);
});

promise.then(function(resultado) {
    console.log(resultado);
}, function(error) {
    console.log("Algo salió mal");
    console.log(error);
}).catch(console.log);

Con este ejemplo, nos aparecerá un alert que al aceptar, resolveremos la promesa
pero, si la rechazamos, usaremos el método reject.

Una cambio en la sintáxis que podemos tener para obtener el resultado de una
promesa es utilizar catch que es lo mismo que pasar una segunda función a then
solo que, es mucho más legible. Ejemplo:

let promise = new Promise(function(resolve, reject) {
    setTimeout(function() {
        if (confirm("¿Esta promesa se cumplió?"))
            return resolve("Hola mundo");
        return reject(new Error("Hubo un error"));
    }, 2000);
});

promise.then(function(resultado) {
    console.log(resultado);
}).catch(function(err) {
    console.log("Algo salió mal");
    console.log(err);
});

Estas son las dos formas en las que podemos enlazar manejadores con una promesa
que se cumplió o se rechazó. Cuando se cumple la promesa, el manejador que
actúa es then y mientras no se cumple el manejador que actúa es catch.